<?php

require_once('animal.php') ;
require_once('frog.php') ;
require_once('ape.php') ;

$object = new animal('shaun');

echo " Name : $object->name . <br>";
echo " Legs $object->legs . <br>";
echo " Cold Blooded : $object->cold_blooded . <br> <br>";


$object1 = new Frog('buduk') ;
{
    
echo " Name : $object1->name . <br>";
echo " Legs : $object1->legs . <br>";
echo " Cold Blooded : $object1->cold_blooded . <br>";
$object1->jump() . "<br> <br>";
}

echo "<br> <br>" ;

$object2 = new Ape('sungokong') ;
{
    
echo " Name : $object2->name . <br>";
echo " Legs : $object2->legs . <br>";
echo " Cold Blooded : $object2->cold_blooded . <br>";
$object2->yell() . "<br> <br>";
}

?>